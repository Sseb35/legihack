
const lex = 'https://archeo-lex.fr/durafront/server/diff'
const dsr = 'http://127.0.0.1:8081/simul_dsr'

module.exports = {
  postDiff: async (texteAmendement = '', texteArticle = '') => {
    return $.post({
      'type': 'POST',
      'url': lex,
      'crossDomain': true,
      'dataType': 'json',
      'timeout': 120*1000,
      'data': JSON.stringify({
        texteAmendement,
        texteArticle
      })
    })
  },
  postDSR: async (L2334_21 = '') => {
    return $.post({
      'type': 'POST',
      'url': dsr,
      'crossDomain': true,
      'dataType': 'json',
      'timeout': 120*1000,
      'data': JSON.stringify({
        L2334_21
      })
    })
  }
}
