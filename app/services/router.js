

const pages = require('../components')

let currentPage = {content:'',name:'dummy'}
let spinnertimeout

const startSpinner = () => {
  if(spinnertimeout) return
  spinnertimeout = setTimeout(() => $('#spinner').fadeIn(), 500)
  setTimeout(() => {
    if(!spinnertimeout) return
    clearTimeout(spinnertimeout)
    spinnertimeout = null
    $('#spinner').hide()
  }, 20000)
}

const stopSpinner = () => {
  if(!spinnertimeout) return
  $('#spinner').hide()
  clearTimeout(spinnertimeout)
  spinnertimeout = null
}

const loadComponent = async (domquery, component, params = []) => {
  startSpinner()
  currentPage = component
  if(navigator.onLine) $('#offline').hide()
  else $('#offline').show()
  $("title").text("ANS · "+component.name)
  $("#pagetitle").text(component.name)
  $(domquery).html(component.content)
  if (component.init) await component.init(...params)
  stopSpinner()
}

const loadPage = async (page) => {
  if (currentPage.exit) await currentPage.exit()
  await loadComponent("#app", pages[page])
  

}

module.exports = {
  loadComponent,
  loadPage
}

