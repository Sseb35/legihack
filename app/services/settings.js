const store = require('./storage')
const settings = store.get('settings') || {}
const subscriptions = {}

module.exports = {
  save: () => {
    store.set('settings', settings)
    store.save()
    Object.values(subscriptions).forEach(f => f())
  },
  set: (name, value) => settings[name] = value,
  get: name => settings[name],
  keys: () => Object.keys(settings),
  onSave: (name, subscribe) => {
    subscriptions[name]=subscribe
  }
}