
import fs from 'fs'
const api = require('../services/api')

const amdts = {}
let ppjl = {};

const init = () => {
  const csv = fs.readFileSync(__dirname + '/../data/amendements2020.csv', 'latin1').split('\n')
  csv.forEach((line, i) => {
    if(line.startsWith('Amt')){
      const tmp = line.split("\t")
      tmp[7] = tmp[7].replace(/<p[^>]*>(.*?)<\/p>/g, "$1\n").replace(/<[^>]*>?/gm, '').replace( /&#([0-9]+);/g, (m,d) => String.fromCharCode(d) ).replace( / /g, " " )
      // create an id
      tmp[0] = tmp[1].replace(/ /g,"").replace(/\./g,"_")
      amdts[tmp[0]] = tmp
    }
  })
  const ppjl_csv = fs.readFileSync(__dirname + '/../data/plf2020.csv', 'utf-8').split('\n')
  ppjl = ppjl_csv.reduce((obj,line) => {
      const tmp = line.split("\t");
      if( tmp.length === 2 ) {
         obj[tmp[0]] = tmp[1].replace(/\\n/g, "\n");
      }
      return obj;
  }, {} );
}

init()


module.exports = {
  search: (text) => {
    const results = []
    Object.values(amdts).forEach(amd => {
      if(amd[2].toLowerCase().search(text)>-1) results.push(amd)
      if(amd[4].toLowerCase().search(text)>-1) results.push(amd)
      if(amd[7].toLowerCase().search(text)>-1) results.push(amd)
      if(amd[8].toLowerCase().search(text)>-1) results.push(amd)
    })
    return results
  },

  sortByPeople: (amds) => {
    const people = {}
    if(!amds) amds = amdts
    Object.values(amds).forEach((line, i) => {
      const person = line[4].replace(/ /g,"_").replace(/\./g,"_")
      if(!people[person]) {
        people[person] = {}
      }
      people[person][line[1]]=line[9]
    })
    return people
  },

  getAmendement: (id) => {
    return amdts[id.replace(/ /g,"").replace(/\./g,"_")]
  },

  diff: idAmendement => {
    const amd = amdts[idAmendement.replace(/ /g,"").replace(/\./g,"_")]
    const nrArticleModifie = amd[2].replace( /^Article /, '' );
    const articleModifie = nrArticleModifie in ppjl ? ppjl[nrArticleModifie] : '';
    return api.postDiff(amd[7],articleModifie)
  },

  diffText: text => {
    return api.postDiff('', text)
  },

  diffDSR: text => api.postDSR( text ),
}
