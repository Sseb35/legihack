const store = JSON.parse(localStorage.getItem('storage') || "{}")
const persistent = {}

module.exports = {
  set: (name, value) => store[name]=value,
  get: (name) => store[name],
  save: () => localStorage.setItem('storage', JSON.stringify(store)),
  removePersistent: (name) => {
    delete persistent[name]
  },
  getPersistent: (name) => {
    return persistent[name]
  },
  setPersistent: (name, value, duration = 10) => {
    persistent[name] = value
    setTimeout(() => delete persistent[name], duration*1000)
  },
}