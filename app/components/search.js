const api = require('../services/api')
const card = require('./card')
const legi = require('../services/legi')

module.exports = {
  name: 'Search',
  icon: 'fa-search',
  content: `
  <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-4">
    <form id="search" class="col-lg-8 offset-lg-2">
      <div class="input-group">
        <input type="text" class="form-control border-0 small" placeholder="Recherchez..." aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-primary"><i class="fas fa-search fa-sm"></i></button>
        </div>
        <div class="input-group-append">
          <button class="btn btn-danger input-group-append" id='clear'>Clear</button>
        </div>
      </div>
    </form>
  </div>
  <div id="result"/>
  `,
  init: async () => {
    $("#clear").click(event => {
      $("#search input").val('')
      $("#result").html('')
    })
    $("#search").submit(async event => {
      event.preventDefault()
      const search = $("#search input").val()
      if(search.length === 0) return
      const results = legi.search(search)
      console.log(results)
      results.forEach(r => card.displayObject('#result', r[1],r))
    })
  }
}