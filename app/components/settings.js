const settings = require('../services/settings')
const store = require('../services/storage')
const api = require('../services/api')

const displayNested = (name, value) => {
  let description
  if(value instanceof Object) {
    description = $(`
      <dl class="row">
        <dt class="col-lg-2 text-left btn btn-link text-truncate" data-toggle="collapse" data-target="#databody${name}" aria-expanded="false" aria-controls="collapse${name}">${name}</dt>
        <dd class="col-lg-10 collapse" id="databody${name}" aria-labelledby="collapse${name}">
        </dd>
      </dl>
    `)
    const nest = description.find("#databody"+name)
    Object.keys(value).forEach(k => nest.append(displayNested(k, value[k])))
  } else {
    description = $(`
    <dl class="row" >
      <dt class="col-lg-2 text-truncate">${name}</dt>
      <dd class="col-lg-10">
      <input class="form-control bg-light border-0 small" id='settings-${name}' value=${value}></dd>
    </dl>
  `)
  }
  return description
}
const displayData = (domquery, title, data) => {
  const card = $(
    `<div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">${title}</h6>
      </div>
      <div class="card-body" id="databody">
      </div>
    </div>
    `
  )
  const nest = card.find("#databody")
  settings.keys().forEach(k => nest.append(displayNested(k, data.get(k))))
  $(domquery).prepend(card)
}

module.exports = {
  name: 'Settings',
  icon: 'fa-cogs',
  content: `
    <div class="col-lg-12 mb-2" id="settings"/>
  `,
  init: async () => {
    displayData('#settings', "Settings", settings)
  },
  //TODO: better handle of network disconnection
  exit: async () => {
    settings.keys().forEach(k => settings.set(k, $("#settings-"+k).val()))
    settings.save()
  }
}