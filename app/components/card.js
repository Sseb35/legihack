
const linkify = (name, value) => {
  if(name.startsWith("I-") || name.startsWith("II-")) {
    return `<a href="#" class = "am-link" data-am="${name}" page="wallet">${value}</a>`
  }
}

const displayNested = (name, value, id) => {
  let description
  if(value instanceof Object) {
    description = $(`
      <dl class="row reduced">
        <dt class="col-lg-4 text-left btn btn-link text-truncate" data-toggle="collapse" data-target="#${name}${id}" aria-expanded="false" aria-controls="${name}${id}">${name}</dt>
        <dd class="col-lg-8 collapse" id="${name}${id}" aria-labelledby="${name}${id}">
      
        </dd>
      </dl>
    `)
    const nest = description.find("#"+name+id)
    Object.keys(value).forEach(k => nest.append(displayNested(k, value[k], id)))
  } else if(value !== null){
    description = $(`
      <dl class="row reduced" >
        <dt class="col-lg-4 text-truncate">${name}</dt>
        <dd class="col-lg-8">${linkify(name,value)}</dd>
      </dl>
    `)
  }
  return description
}
const displayObject = (domquery, title, data) => {
  if(Object.keys(data).length === 0) return
  if(data instanceof Array) return displayArray(domquery, title, data)
  const random = Buffer.from(Math.random().toString()).toString('hex')
  const card = $(`
  <div class="col-lg-12" id=${random}>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">${title}</h6>
      </div>
      <div class="card-body" id="databody">
      </div>
    </div>
  </div>
  `)
  const nest = card.find("#databody")
  Object.keys(data).forEach(k => nest.append(displayNested(k, data[k], data.id)))
  $(domquery).prepend(card)
  $('#'+random+' .walletlink').click(event => {
    const page = $(event.currentTarget).attr('page')
    const component = require('./'+page)
    require('../services/router').loadComponent("#app", component, [$(event.currentTarget).text()])
  })
}


displayArray = (domquery, title, data) => {
  if(Object.keys(data).length === 0) return
  const random = Buffer.from(Math.random().toString()).toString('hex')
  const card = $(`
  <div class="col-lg-12" id=${random}>
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <a class="m-0 am-link font-weight-bold text-primary" data-id="${data[0]}">${title}</ha>
      </div>
      <div class="card-body table-responsive" id="databody">
      <dl class="row reduced" >
        <dt class="col-lg-4 text-truncate">Date</dt>
        <dd class="col-lg-8">${data[6]}</dd>
      </dl>
      <dl class="row reduced" >
        <dt class="col-lg-4 text-truncate">Propose</dt>
        <dd class="col-lg-8">${data[4]}</dd>
      </dl>
      <dl class="row reduced" >
        <dt class="col-lg-4 text-truncate">Article</dt>
        <dd class="col-lg-8">${data[2]} - ${data[3]}</dd>
      </dl>
      <dl class="row reduced" >
        <dt class="col-lg-4 text-truncate">Disposition</dt>
        <dd class="col-lg-8">${data[7]}</dd>
      </dl>
      </div>
    </div>
  </div>
  `)
  $(domquery).append(card)
  $('#'+random+' .am-link').click(event => {
    const idam = $(event.currentTarget).attr('data-id')
    const component = require('./amendement')
    require('../services/router').loadComponent("#app", component, [idam])
  })
}

module.exports = {
  displayObject,
  displayArray
}