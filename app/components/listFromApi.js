const api = require('../services/api')
const card = require('./card')

module.exports = (label, icon, apifunc, params, sort, id) => {
  return {
    name: label,
    icon: icon,
    content: `
      <div id="card-data" class="mt-2"></div>
    `,
    init: async () => {
      let results = await api[apifunc](...params)
      if(sort) results = results.sort(sort)
      results.forEach(d => {
        if(!d.id) d.id=Buffer.from(d[id]).toString('hex')
        card.displayObject("#card-data", d[id], d)
      })
    }
  }
  
}