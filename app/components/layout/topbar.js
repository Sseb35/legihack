
const settings = require('../../services/settings')
const store = require('../../services/storage')

module.exports = {
  content:`
  <nav class="fixed-top navbar navbar-expand navbar-light bg-white topbar static-top">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>
    <i class="fas fa-angle-right navbar-nav"></i>&nbsp;<h5 id="pagetitle" class="mr-auto navbar-nav text-truncate"></h5>
    <ul class="navbar-nav">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="fa-stack fa-sm mr-2">
          <i class="fas fa-signal fa-stack-1x text-primary"></i>
          <i id="offline" class="fas fa-ban fa-stack-2x fa-inverse text-danger"></i>
        </span>
          <span id='connectednetwork' class="mr-2 d-none d-lg-inline text-gray-600 small">Info</span>
        </a>
      </li>
    </ul>
  </nav>
  `,
  init: async () => {
    $("#sidebarToggleTop").on('click', (e) => {
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide')
      }
    })
  } 
}
