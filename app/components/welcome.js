const legi = require('../services/legi')
const card = require('./card')

module.exports = {
  name: 'Home',
  icon: 'fa-home',
  content: `
  <div class="row">
    <div class="col-lg-12 mt-2">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Présentation</h6>
        </div>
        <div class="card-body">
          <p>Dans cette démonstration, seuls les amendements de M. Hackathon n° II-1999 et M. Cuypers n° I-2 rect. fonctionnent.</p>
          <p>Le résultat du premier était l’objet du hackathon Datafin : à partir d’un amendement portant article additionnel et modifiant un des paramètres de la dotation DSR, calculer le texte modifié dans le CGCT, puis calculer les effets sur les communes et notamment leur éligibilité à cette dotation.</p>
          <p>Les deux amendements utilisent le programme <a href="https://github.com/Legilibre/DuraLex">programme DuraLex</a> pour le calcul du “diff” (différences entre le texte actuel et le texte amendé). La simulation de l’éligibilité à la DSR a fait l’objet d’un <a href="https://github.com/Alexander1842/Datafin2020">programme dédié</a> écrit pendant le hackathon. Cette interface a aussi été créée pendant le hackathon (sources <a href="https://gitlab.com/fix/legihack">sur ce lien</a> et sur <a href="https://gitlab.com/Sseb35/legihack">celui-ci</a>).</p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6 mt-2" id="amendements">
    </div>
    <div class="col-lg-6 mt-2">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Amendement</h6>
        </div>
        <div style="min-height:60vh;" class="card-body" id="result">
        </div>
      </div>
    </div>
  </div>
  `,
  init: async () => {
    const people = legi.sortByPeople()
    card.displayObject('#amendements','Sénateur',people)
    $(".am-link").click(async e => {
      const am = $(e.currentTarget).attr('data-am')
      console.log(am)
      const amdt = legi.getAmendement( am )
      if( /^art\. add\. /.test( amdt[2] ) ) {
        $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>" ) + "</p><h4>Effets de l’amendement portant article additionnel :</h4><p>Calcul en cours…</p>" )
	const artAdd = amdt[7].replace( /^Après +l['’] *article +[0-9]+\n+Insérer +un +article( +additionnel)? +ainsi +rédigé *:? *\n+/, "" );
        let resultCode = false;
        try {
          resultCode = await legi.diffText(artAdd);
        } catch(e) {}
        if( resultCode && resultCode.data && resultCode.data["code code général des collectivités territoriales"] && resultCode.data["code code général des collectivités territoriales"]["L2334-21"] && resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text ) {
          $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>" ) + "</p><h4>Effets de l’amendement portant article additionnel :</h4><p>" + resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text.replace(/\n+/g, "</p><p>" ).replace( /<ins /g, "<ins style=\"background-color:chartreuse\" " ).replace( /<del /g, "<del style=\"background-color:lightsalmon\" " ).replace( /<\/ins>/g, "</ins> " ) + "</p><h4>Effets sur les communes</h4><p>Calcul en cours…</p>" );
          console.log( resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text.replace( /<ins[^>]*>/g, "" ).replace( /<\/ins>/g, " " ).replace( /<del[^>]*>.*?<\/del>/g, "" ) );
          let resultDSR = false;
          try {
            resultDSR = await legi.diffDSR( resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text.replace( /<ins[^>]*>/g, "" ).replace( /<\/ins>/g, " " ).replace( /<del[^>]*>.*?<\/del>/g, "" ).replace( /’/g, "'" ) );
          } catch(e) {}
          console.log( resultDSR );
          if( resultDSR ) {
            const tableau = Object.keys(resultDSR).sort().reduce( (res, commune) => {
              return res + "<tr>" +
                "<td>" + resultDSR[commune].nom_com + "(" + commune + ")</td>" +
                ( resultDSR[commune].avant.elig_frac_1 ? "<td style=\"background-color:chartreuse\">vrai</td>" : "<td style=\"background-color:lightsalmon\">faux</td>" ) +
                ( resultDSR[commune].avant.elig_generale ? "<td style=\"background-color:chartreuse\">vrai</td>" : "<td style=\"background-color:lightsalmon\">faux</td>" ) +
                ( resultDSR[commune].apres.elig_frac_1 ? "<td style=\"background-color:chartreuse\">vrai</td>" : "<td style=\"background-color:lightsalmon\">faux</td>" ) +
                ( resultDSR[commune].apres.elig_generale ? "<td style=\"background-color:chartreuse\">vrai</td>" : "<td style=\"background-color:lightsalmon\">faux</td>" ) +
                "</tr>";
            }, "");
            const legende = "<p>(1) = éligibilité première fraction</p><p>(2) = éligibilité générale</p>";
            const entete = "<tr><td rowspan=\"2\">Commune (code INSEE)</td><td colspan=\"2\">Actuel</td><td colspan=\"2\">Amdt</td></tr><tr><td>(1)</td><td>(2)</td><td>(1)</td><td>(2)</td></tr>";
            $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>" ) + "</p><h4>Effets de l’amendement portant article additionnel :</h4><p>" + resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text.replace(/\n+/g, "</p><p>" ).replace( /<ins /g, "<ins style=\"background-color:chartreuse\" " ).replace( /<del /g, "<del style=\"background-color:lightsalmon\" " ).replace( /<\/ins>/g, "</ins> " ) + "</p><h4>Effets sur les communes</h4>" + legende + "<p><table border=\"1\">" + entete + tableau + "</table></p>" );
          } else {
            $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>" ) + "</p><h4>Effets de l’amendement portant article additionnel :</h4><p>" + resultCode.data["code code général des collectivités territoriales"]["L2334-21"].text.replace(/\n+/g, "</p><p>" ).replace( /<ins /g, "<ins style=\"background-color:chartreuse\" " ).replace( /<del /g, "<del style=\"background-color:lightsalmon\" " ).replace( /<\/ins>/g, "</ins> " ) + "</p><h4>Effets sur les communes</h4><p>Calcul échoué.</p>" );
          }
	} else {
          $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>") + "</p><h4>Effets de l’amendement portant article additionnel :</h4><p>Calcul échoué.</p>" );
	}
      } else {
        $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>" ) + "</p><h4>Effets de l’amendement sur le projet de loi :</h4><p>Calcul en cours…</p>" )
        const result = await legi.diff(am)
        if( result && result.data && result.data["anonymous law"] && result.data["anonymous law"]["anonymous article"] && result.data["anonymous law"]["anonymous article"].text ) {
          $('#result').html( "<p>" + amdt[7].replace(/\n+/g, "</p><p>") + "</p><h4>Effets de l’amendement sur le projet de loi :</h4><p>" + result.data["anonymous law"]["anonymous article"].text.replace(/\n+/g, "</p><p>").replace( /<ins /g, "<ins style=\"background-color:chartreuse\" " ).replace( /<del /g, "<del style=\"background-color:lightsalmon\" " ) + "</p>" );
        }
      }
    })
  }
}
