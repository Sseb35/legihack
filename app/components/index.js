const welcome = require('./welcome')
const search = require('./search')
const settings = require('./settings')
const about = require('./about')
const sidebar = require('./layout/sidebar')
const footer = require('./layout/footer')
const topbar = require('./layout/topbar')
const listFromApi = require('./listFromApi')

module.exports = {
  welcome,
  settings,
  search,
  about,
  layout: {
    sidebar,
    footer,
    topbar
  }
  
}