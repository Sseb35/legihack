const legi = require('../services/legi')
const card = require('./card')

module.exports = {
  name: 'Amendement',
  icon: 'fa-home',
  content: `
  <div class="row">
    <div class="col-lg-6 mt-2" id="amendement">
    </div>
    <div class="col-lg-6 mt-2">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Amendement</h6>
        </div>
        <div style="min-height:60vh;" class="card-body" id="result">
        </div>
      </div>
    </div>
  </div>
  `,
  init: async (id) => {
    const amd = legi.getAmendement(id)
    console.log(id)
    card.displayObject('#amendement','Amendement',amd)
    $(".am-link").click(async e => {
      const am = $(e.currentTarget).attr('data-am')
      console.log(am)
      const result = await legi.diff(am)
      $('#result').html(JSON.stringify(result))
    })
  }
}