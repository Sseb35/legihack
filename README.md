Quickstart
----------
1. install nodejs
2. npm install
3. npm start

open browser at http://localhost:1234

La partie fonctionnelle pour démonstration est :
* cliquer sur « Home » à gauche
* sélectionner l’amendement de M. Hackathon n° II-1999 ou l’amendement de M. Cuypers n° I-2 rect.
* il devrait s’afficher à droite l’amendement et ses effets sur le texte

Development
-----------

- call to the archeo-lex API: app/services/api
- UI for the API call: app/components/welcome.js

In case of issue with the app, remove the parcel cache and restart:
- rm -fr .cache/
- npm start
